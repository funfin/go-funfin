# go funfin

        go mod init gitlab.com/funfin/go-funfin


* local

        go build -o bin/go-funfin -v . && \
        go install && \
        PORT=5005 heroku local web

* deploy to heroku

        heroku create go-funfin
        heroku create go-funfin-stage

        heroku pipelines:create -a go-funfin
        heroku pipelines:add go-funfin -a go-funfin-stage

        heroku config:set PORT=8080 -a go-funfin
        heroku config:set PORT=8080 -a go-funfin-stage
