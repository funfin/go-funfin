package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/funfin/go-funfin/lissajous"
	"gitlab.com/funfin/go-funfin/mandelbrot"
	"gitlab.com/funfin/go-funfin/surface"
)

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/lissajous", lissajousHandler)
	http.HandleFunc("/surface", surfaceHandler)
	http.HandleFunc("/mandelbrot", mandelbrotHandler)

	port := os.Getenv("PORT")

	log.Printf("Listening on port: %v", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `
<html>
<head>
<title>go-funfin</title>
</head>
<body>

<h1>go-funfin</h1>
<p><a href="/lissajous">lissajous</a></p>
<p><a href="/surface">surface</a></p>
<p><a href="/mandelbrot">mandelbrot</a></p>

</body>
</html>
	`)
}

func lissajousHandler(w http.ResponseWriter, r *http.Request) {
	lissajous.Lissajous(w)
}

func surfaceHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	surface.Render(w)
}

func mandelbrotHandler(w http.ResponseWriter, r *http.Request) {
	mandelbrot.Render(w)
}
